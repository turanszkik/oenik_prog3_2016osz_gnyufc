var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwx",
  1: "abdefghiklmprstuv",
  2: "tx",
  3: "abdefghiklmprstuv",
  4: "abcdefghiklmoprstuw",
  5: "_defghiklmnprstuv",
  6: "bcdefghiklmprstuv",
  7: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties",
  7: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties",
  7: "Events"
};

