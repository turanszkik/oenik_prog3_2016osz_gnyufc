﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Turanszki_Kristof_Beadando.Logika;
using Turanszki_Kristof_Beadando.Palyaelemek;

namespace Turanszki_Kristof_Beadando.Converter
{
    /// <summary>
    /// Ez az osztály felelő azért, hogy az értékül kapott PalyaModel-t konvertálja DrawingGroup.Children-né és így megjelenjen a program ablakában.
    /// </summary>
    internal class VMToGeometryDrawingConverter : IValueConverter
    {
        /// <summary>
        /// A kapott osztályt (PalyaModel) konvertálja át DrawingGroup.Children-né
        /// </summary>
        /// <param name="value">PalyaModel (mint VM)</param>
        /// <param name="targetType">The parameter is not used.</param>
        /// <param name="parameter">The parameter is not used.</param>
        /// <param name="culture">The parameter is not used.</param>
        /// <returns>DrawingGroup .Children</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            PalyaModel vM = (PalyaModel)value;
            DrawingGroup dG = new DrawingGroup();

            Rect h = new Rect(0, 0, Hatterkep.SZELESSEG, Hatterkep.MAGASSAG);
            dG.Children.Add(new GeometryDrawing(vM.Hatter.Brush, null, new RectangleGeometry(h)));
            for (int i = 0; i < vM.Powerupok.Count; i++)
            {
                dG.Children.Add(new GeometryDrawing(vM.Powerupok[i].Brush, null, vM.Powerupok[i].Geometry));
            }

            for (int i = 0; i < vM.Falak.Count; i++)
            {
                dG.Children.Add(new GeometryDrawing(vM.Falak[i].Brush, null, vM.Falak[i].Geometry));
            }
            
            dG.Children.Add(new GeometryDrawing(vM.Uto.Brush, null, vM.Uto.Geometry));

            for (int i = 0; i < vM.Labdak.Count; i++)
            {
                dG.Children.Add(new GeometryDrawing(vM.Labdak[i].Brush, null, vM.Labdak[i].Geometry));
            }

            if (vM.Pajzs != null)
            {
                dG.Children.Add(new GeometryDrawing(vM.Pajzs.Brush, null, vM.Pajzs.Geometry));
            }

            if (vM.Toltenyek != null)
            {
                foreach (Tolteny tolteny in vM.Toltenyek)
                {
                    dG.Children.Add(new GeometryDrawing(tolteny.Brush, null, tolteny.Geometry));
                }
            }

            FormattedText ft = new FormattedText("Score: " + vM.Pontok, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Atial"), 30, Brushes.Red);
            dG.Children.Add(new GeometryDrawing(Brushes.Red, null, ft.BuildGeometry(new Point(0, 0))));
            ft = new FormattedText("Extra Lives: " + vM.Eletek, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Atial"), 30, Brushes.Red);
            dG.Children.Add(new GeometryDrawing(Brushes.Red, null, ft.BuildGeometry(new Point((Hatterkep.SZELESSEG / 2) - 80, 0))));

            return new DrawingImage(dG);
        }

        /// <summary>
        /// Nem használjuk, mert sosem fogunk DrawingGroup.Children-ből PalyaModel-t csinálni
        /// </summary>
        /// <param name="value">The parameter is not used.</param>
        /// <param name="targetType">The parameter is not used.</param>
        /// <param name="parameter">The parameter is not used.</param>
        /// <param name="culture">The parameter is not used.</param>
        /// <returns>Nincsen használva</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
