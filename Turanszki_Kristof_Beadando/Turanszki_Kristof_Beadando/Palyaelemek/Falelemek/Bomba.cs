﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Turanszki_Kristof_Beadando.Palyaelemek
{
    /// <summary>
    /// Bomba típusú fal elem.
    /// </summary>
    internal class Bomba : Fal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bomba" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public Bomba(Point hely)
        {
            Hely = hely;
        }

        /// <summary>
        /// Ez a metódus felelős a külső eléréshez, és a kép nevét itt adjuk át.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return GetBrush("Bomba.png");
            }
        }

        /// <summary>
        /// Visszaadja, hogy mit csinál, ha érintkezik a labdával.
        /// </summary>
        /// <param name="bomba">Bomba-e a megadott elem</param>
        /// <returns>true = széttört , false = nem tört szét</returns>
        public override bool Torik(bool bomba = true)
        {
            return true;
        }
    }
}
