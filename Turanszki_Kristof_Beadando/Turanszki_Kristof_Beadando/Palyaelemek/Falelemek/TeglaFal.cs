﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Turanszki_Kristof_Beadando.Palyaelemek
{
    /// <summary>
    /// Tégla tipusú fal elem
    /// </summary>
    internal class TeglaFal : Fal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TeglaFal" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public TeglaFal(Point hely)
        {
            this.Hely = hely;
        }

        /// <summary>
        /// Ez a metódus felelős a külső eléréshez, és a kép nevét itt adjuk át.
        /// </summary>
        public override Brush Brush
        {
            get { return GetBrush("TeglaFal.png"); }
        }

        /// <summary>
        /// Megadja, hogy széttörik-e az elem
        /// </summary>
        /// <param name="bomba">Bomba robbantja-e?</param>
        /// <returns>true = széttörik, false = nem törik szét</returns>
        public override bool Torik(bool bomba = false)
        {
            return true;
        }
    }
}
