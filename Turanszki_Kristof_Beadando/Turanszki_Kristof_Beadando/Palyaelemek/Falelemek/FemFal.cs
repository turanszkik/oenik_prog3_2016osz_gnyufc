﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Turanszki_Kristof_Beadando.Palyaelemek
{
    /// <summary>
    /// Fém típusú fal elem
    /// </summary>
    internal class FemFal : Fal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FemFal" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public FemFal(Point hely)
        {
            Hely = hely;
        }

        /// <summary>
        /// Ez a metódus felelős a külső eléréshez, és a kép nevét itt adjuk át.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return GetBrush("FemFal.png");
            }
        }

        /// <summary>
        /// Megadja, hogy széttörik-e az elem
        /// </summary>
        /// <param name="bomba">Bomba robbantja-e?</param>
        /// <returns>true = széttörik, false = nem törik szét</returns>
        public override bool Torik(bool bomba = false)
        {
            if (bomba)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
