﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Turanszki_Kristof_Beadando.Interface;
using Turanszki_Kristof_Beadando.Logika;

namespace Turanszki_Kristof_Beadando.Palyaelemek
{
    /// <summary>
    /// A fal elemek egy absztrakt osztlya, mely minden falelem őse
    /// </summary>
    internal abstract class Fal : Bindable, IUtkozhet
    {
        /// <summary>
        /// Az elem szélességét haztározza meg.
        /// </summary>
        public const int SZELESSEG = Hatterkep.SZELESSEG / 25;

        /// <summary>
        /// Az elem magasságát határozza meg.
        /// </summary>
        public const int MAGASSAG = Hatterkep.MAGASSAG / 17;

        /// <summary>
        /// Ütközés szempontjából használt
        /// </summary>
        private Geometry geometry;

        /// <summary>
        /// Az elem helyét határozza meg.
        /// </summary>
        private Point hely;

        /// <summary>
        /// Beállítja, és lekérdezi az elem helyét.
        /// </summary>
        public Point Hely
        {
            get
            {
                return hely;
            }

            set
            {
                hely = value;
            }
        }

        /// <summary>
        /// Visszaadja a szélességét az elemnek
        /// </summary>
        public int Szelesseg
        {
            get
            {
                return SZELESSEG;
            }
        }

        /// <summary>
        /// Visszaadja a magasságát az elemnek
        /// </summary>
        public int Magassag
        {
            get
            {
                return MAGASSAG;
            }
        }

        /// <summary>
        /// Ez a metódus felelős a külső eléréshez.
        /// </summary>
        public abstract Brush Brush { get; }

        /// <summary>
        /// Visszaadja minden fel elem geometry-jét
        /// </summary>
        public Geometry Geometry
        {
            get
            {
                if (geometry == null)
                {
                    geometry = new RectangleGeometry(new Rect(Hely.X, Hely.Y, SZELESSEG, MAGASSAG));
                }

                return geometry;
            }
        }

        /// <summary>
        /// Akkor hívandó meg, mikor eltalálja a labda
        /// </summary>
        /// <param name="bomba">Bomba robbantja-e?</param>
        /// <returns>true = széttört , false = nem tört szét</returns>
        public abstract bool Torik(bool bomba = false);

        /// <summary>
        /// Az elemek megjelenítéséért felelős metódus
        /// </summary>
        /// <param name="fname">A magjelenítendő kép neve</param>
        /// <returns>A brush-t adja vissza</returns>
        protected Brush GetBrush(string fname)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(fname, UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            ib.Viewport = new Rect(Hely.X, Hely.Y, SZELESSEG, MAGASSAG);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }
    }
}
