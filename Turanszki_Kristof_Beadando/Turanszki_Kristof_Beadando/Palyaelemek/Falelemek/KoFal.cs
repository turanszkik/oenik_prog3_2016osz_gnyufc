﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Turanszki_Kristof_Beadando.Palyaelemek
{
    /// <summary>
    /// Ko tipusú fel elem
    /// </summary>
    internal class KoFal : Fal
    {
        /// <summary>
        /// Ha már egyszer eltalálták golyó által, ez true lesz, addig false.
        /// </summary>
        private bool repedt;

        /// <summary>
        /// Initializes a new instance of the <see cref="KoFal" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public KoFal(Point hely)
        {
            repedt = false;
            this.Hely = hely;
        }

        /// <summary>
        /// Getter Setter metódus a repedt értékhez.
        /// </summary>
        public bool Repedt
        {
            get
            {
                return repedt;
            }

            set
            {
                repedt = value;
            }
        }

        /// <summary>
        /// Ez a metódus felelős a külső eléréshez, és a kép nevét itt adjuk át.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                if (Repedt)
                {
                    return GetBrush("TorottKoFal.png");
                }
                else
                {
                    return GetBrush("KoFal.png");
                }
            }
        }

        /// <summary>
        /// Megadja, hogy széttörik-e az elem
        /// </summary>
        /// <param name="bomba">Bomba robbantja-e?</param>
        /// <returns>true = széttörik, false = nem törik szét</returns>
        public override bool Torik(bool bomba = false)
        {
            if (bomba)
            {
                repedt = true;
            }

            if (Repedt)
            {
                return true;
            }
            else
            {
                repedt = true;
                return false;
            }
        }
    }
}
