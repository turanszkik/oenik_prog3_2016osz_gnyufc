﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Turanszki_Kristof_Beadando.Interface;
using Turanszki_Kristof_Beadando.Logika;

namespace Turanszki_Kristof_Beadando.Palyaelemek
{
    /// <summary>
    /// Az ütő adatainak tárolásáért felelős osztály
    /// </summary>
    internal class Uto : Bindable, IUtkozhet
    {
        /// <summary>
        /// Az ütő magasságát tárolja
        /// </summary>
        public const int MAGASSAG = 20;

        /// <summary>
        /// Az ütő szélessége
        /// </summary>
        private int szelesseg;

        /// <summary>
        /// Azt tárolja, hogy a mágnes powerup aktív-e
        /// </summary>
        private bool magnes = false;

        /// <summary>
        /// Az ütő helyét tárolja
        /// </summary>
        private Point hely;

        /// <summary>
        /// Ütközés szempontjából használt
        /// </summary>
        private Geometry geometry;

        /// <summary>
        /// Az ütő középpontjának helyét tárolja
        /// </summary>
        private Point kozep;

        /// <summary>
        /// Initializes a new instance of the <see cref="Uto" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public Uto(Point hely)
        {
            Hely = hely;
            szelesseg = 200;
        }

        /// <summary>
        /// Getter Setter az ütő helyéhez lekérdezésre, értékadásra
        /// </summary>
        public Point Hely
        {
            get
            {
                return hely;
            }

            set
            {
                double x = value.X;
                value.X -= Szelesseg / 2;
                if (value.X + Szelesseg < 1400 - Tolteny.SZELESSEG)
                {
                    if (value.X >= Tolteny.SZELESSEG)
                    {
                        hely = value;
                        kozep.X = x;
                        kozep.Y = value.Y + (Magassag / 2);
                    }
                    else
                    {
                        value.X = Tolteny.SZELESSEG;
                        hely = value;
                    }
                }
                else
                {
                    value.X = 1400 - Tolteny.SZELESSEG - Szelesseg;
                    hely = value;
                }
            }
        }

        /// <summary>
        /// Getter Szetter a szélesség lekérdezésére, megadására
        /// </summary>
        public int Szelesseg
        {
            get
            {
                return szelesseg;
            }

            set
            {
                szelesseg = value;
            }
        }

        /// <summary>
        /// Az elemek megjelenítéséért felelős
        /// </summary>
        public Brush Brush
        {
            get
            {
                ImageBrush ib = new ImageBrush(new BitmapImage(new Uri("Uto.png", UriKind.Relative)));
                ib.TileMode = TileMode.Tile;
                ib.Viewport = new Rect(Hely.X, Hely.Y, Szelesseg, MAGASSAG);
                ib.ViewportUnits = BrushMappingMode.Absolute;
                return ib;
            }
        }

        /// <summary>
        /// Getter a magasság lekérdezésére
        /// </summary>
        public int Magassag
        {
            get
            {
                return MAGASSAG;
            }
        }

        /// <summary>
        /// Getter Setter az ütő középpontjának lekérdezésére, értékadására
        /// </summary>
        public Point Kozep
        {
            get
            {
                return kozep;
            }

            private set
            {
                kozep = value;
            }
        }

        /// <summary>
        /// Getter Setter a mágnesesség lekérdezése, értékadása
        /// </summary>
        public bool Magnes
        {
            get
            {
                return magnes;
            }

            set
            {
                magnes = value;
            }
        }

        /// <summary>
        /// Visszaadja minden fel elem geometry-jét
        /// </summary>
        public Geometry Geometry
        {
            get
            {
                geometry = new RectangleGeometry(new Rect(Hely.X, Hely.Y, Szelesseg, MAGASSAG));
                return geometry;
            }
        }
    }
}
