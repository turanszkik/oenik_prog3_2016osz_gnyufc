﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Turanszki_Kristof_Beadando.Interface;
using Turanszki_Kristof_Beadando.Logika;

namespace Turanszki_Kristof_Beadando.Palyaelemek
{
    /// <summary>
    /// A labda adatainak eltárolására szolgáló osztály
    /// </summary>
    internal class Labda : Bindable, IUtkozhet
    {
        /// <summary>
        /// A labda Mérete
        /// </summary>
        public const int MERET = 10;

        /// <summary>
        /// A labda mozgási iránya VEKTORBAN eltárolva
        /// </summary>
        private Vector irany;

        /// <summary>
        /// Ütközés szempontjából használt
        /// </summary>
        private Geometry geometry;

        /// <summary>
        /// A labda KÖZÉPPONTJÁNAK helye
        /// </summary>
        private Point hely;

        /// <summary>
        /// True = A labda halad
        /// False = A labda álló helyzetben van (esetleg az ütőt követi)
        /// </summary>
        private bool halad = false;

        /// <summary>
        /// A labda sebessége
        /// </summary>
        private int sebesseg;

        /// <summary>
        /// Initializes a new instance of the <see cref="Labda" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public Labda(Point hely)
        {
            Hely = hely;
            Sebesseg = 5;
            Irany = new Vector(0, -1);
        }

        /// <summary>
        /// Getter Settter a hely beállításához, és lekérdezéséhez
        /// </summary>
        public Point Hely
        {
            get
            {
                return hely;
            }

            set
            {
                hely = value;
            }
        }

        /// <summary>
        /// Az elemek megjelenítéséért felelős metódus
        /// </summary>
        public Brush Brush
        {
            get
            {
                ImageBrush ib = new ImageBrush(new BitmapImage(new Uri("Labda.png", UriKind.Relative)));
                ib.TileMode = TileMode.Tile;
                ib.Viewport = new Rect(Hely.X - MERET, Hely.Y - MERET, MERET * 2, MERET * 2);
                ib.ViewportUnits = BrushMappingMode.Absolute;
                return ib;
            }
        }

        /// <summary>
        /// Getter Setter a labda irányának beállításához
        /// </summary>
        public Vector Irany
        {
            get
            {
                return irany;
            }

            set
            {
                irany = value;
                double hossz = irany.Length;
                irany.X /= hossz;
                irany.Y /= hossz;
            }
        }

        /// <summary>
        /// Getter Setter a sebesség beállításához, lekérdezéséhez.
        /// </summary>
        public int Sebesseg
        {
            get
            {
                return sebesseg;
            }

            set
            {
                sebesseg = value;
            }
        }

        /// <summary>
        /// Getter Setter a halad érték beállításához, lekérdezéséhez.
        /// </summary>
        public bool Halad
        {
            get
            {
                return halad;
            }

            set
            {
                halad = value;
            }
        }

        /// <summary>
        /// Getter a szélesség lekérdezéséhez.
        /// </summary>
        public int Szelesseg
        {
            get
            {
                return MERET;
            }
        }

        /// <summary>
        /// Getter a magasság lekérdezéséhez.
        /// </summary>
        public int Magassag
        {
            get
            {
                return MERET;
            }
        }

        /// <summary>
        /// Visszaadja minden fel elem geometry-jét
        /// </summary>
        public Geometry Geometry
        {
            get
            {
                geometry = new EllipseGeometry(Hely, MERET, MERET);
                return geometry;
            }
        }

        /// <summary>
        /// Vizsgálja, hogy ütközik-e a képernyő szélével
        /// </summary>
        /// <param name="pont">A mozgás utáni hely</param>
        /// <returns>
        /// Bal = A képernyő bal szélével ütközött
        /// Jobb = A képernyő jobb szélével ütközött
        /// Fent = a felső széllel ütközött
        /// null = nem ütközött
        /// </returns>
        public string Utkozes(Point pont)
        {
            if (pont.X - MERET < 0)
            {
                return "Bal";
            }
            else if (pont.X + MERET > Hatterkep.SZELESSEG)
            {
                return "Jobb";
            }
            else if (pont.Y - MERET < 0)
            {
                return "Fent";
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// A labda mozgásáért felelős metódus.
        /// </summary>
        public void Mozog()
        {
            if (Halad)
            {
                Point pont = new Point(Hely.X + (Irany.X * Sebesseg), Hely.Y + (Irany.Y * Sebesseg));
                switch (Utkozes(pont))
                {
                    case "Bal":
                        irany.X = -Irany.X;
                        Mozog();
                        break;
                    case "Jobb":
                        irany.X = -Irany.X;
                        Mozog();
                        break;
                    case "Fent":
                        irany.Y = -Irany.Y;
                        Mozog();
                        break;
                    default:
                        Hely = pont;
                        break;
                }
            }
        }

        /// <summary>
        /// A metódus lemásolja az értékül kapott labda adatait.
        /// </summary>
        /// <param name="labda">Másolandó labda</param>
        public void Masol(Labda labda)
        {
            Hely = labda.Hely;
            Halad = labda.Halad;
            Irany = labda.Irany;
            Sebesseg = labda.Sebesseg;
        }
    }
}
