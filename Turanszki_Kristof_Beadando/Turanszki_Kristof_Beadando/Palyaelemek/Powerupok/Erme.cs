﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Turanszki_Kristof_Beadando.Logika;

namespace Turanszki_Kristof_Beadando.Palyaelemek.Powerupok
{
    /// <summary>
    /// Érme típusú powerup
    /// </summary>
    internal class Erme : Powerup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Erme" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public Erme(Point hely)
        {
            Hely = hely;
        }
        
        /// <summary>
        /// Ez a metódus felelős a külső eléréshez, és a kép nevét itt adjuk át.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return GetBrush("Erme.png");
            }
        }

        /// <summary>
        /// Pontszámot növel 5-tel metódushívás esetén.
        /// </summary>
        /// <param name="vM">VM átadása</param>
        public override void Aktival(PalyaModel vM)
        {
            vM.Pontok += 5;
        }
    }
}
