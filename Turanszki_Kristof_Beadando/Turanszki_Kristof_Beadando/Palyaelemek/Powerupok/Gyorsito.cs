﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Turanszki_Kristof_Beadando.Logika;

namespace Turanszki_Kristof_Beadando.Palyaelemek.Powerupok
{
    /// <summary>
    /// Gyorsító típusú powerup
    /// </summary>
    internal class Gyorsito : Powerup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Gyorsito" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public Gyorsito(Point hely)
        {
            this.Hely = hely;
        }

        /// <summary>
        /// Ez a metódus felelős a külső eléréshez, és a kép nevét itt adjuk át.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return GetBrush("Gyorsito.png");
            }
        }

        /// <summary>
        /// A jelenlegi labdák sebességét növeli meg (3-mal) metódushíváskor.
        /// </summary>
        /// <param name="vM">VM átadása</param>
        public override void Aktival(PalyaModel vM)
        {
            for (int i = 0; i < vM.Labdak.Count; i++)
            {
                vM.Labdak[i].Sebesseg += 3;
            }
        }
    }
}
