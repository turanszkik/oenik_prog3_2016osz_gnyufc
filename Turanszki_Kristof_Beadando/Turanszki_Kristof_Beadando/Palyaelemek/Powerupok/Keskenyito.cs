﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Turanszki_Kristof_Beadando.Logika;

namespace Turanszki_Kristof_Beadando.Palyaelemek.Powerupok
{
    /// <summary>
    /// Keskenyítő típusú powerup
    /// </summary>
    internal class Keskenyito : Powerup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Keskenyito" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public Keskenyito(Point hely)
        {
            Hely = hely;
        }

        /// <summary>
        /// Ez a metódus felelős a külső eléréshez, és a kép nevét itt adjuk át.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return GetBrush("Keskenyito.png");
            }
        }

        /// <summary>
        /// Az ütő szélességét csökkenti 30 pixellel metódushívás esetén.
        /// </summary>
        /// <param name="vM">VM átadása</param>
        public override void Aktival(PalyaModel vM)
        {
            vM.Uto.Szelesseg -= 30;
            for (int i = 1; i < vM.Toltenyek.Count; i++)
            {
                vM.Toltenyek[i].ModisitX(-30);
                i++;
            }
        }
    }
}
