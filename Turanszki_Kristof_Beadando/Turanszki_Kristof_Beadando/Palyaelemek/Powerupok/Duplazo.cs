﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Turanszki_Kristof_Beadando.Logika;

namespace Turanszki_Kristof_Beadando.Palyaelemek.Powerupok
{
    /// <summary>
    /// Duplazo típusú powerup
    /// </summary>
    internal class Duplazo : Powerup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Duplazo" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public Duplazo(Point hely)
        {
            Hely = hely;
        }

        /// <summary>
        /// Ez a metódus felelős a külső eléréshez, és a kép nevét itt adjuk át.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return GetBrush("Duplazo.png");
            }
        }

        /// <summary>
        /// Megduplázza a jelenlegi labdák számát metódushívás esetén.
        /// </summary>
        /// <param name="vM">VM átadása</param>
        public override void Aktival(PalyaModel vM)
        {
            int db = vM.Labdak.Count;
            for (int i = 0; i < db; i++)
            {
                Vector s = vM.Labdak[i].Irany;
                Labda labda = new Labda(new Point());
                labda.Masol(vM.Labdak[i]);
                Vector v = vM.Labdak[i].Irany;
                v.X += 0.2;
                vM.Labdak[i].Irany = v;
                v = labda.Irany;
                v.X -= 0.2;
                labda.Irany = v;
                vM.Labdak.Add(labda);
            }
        }
    }
}
