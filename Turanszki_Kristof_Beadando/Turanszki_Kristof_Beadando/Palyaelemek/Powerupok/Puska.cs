﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Turanszki_Kristof_Beadando.Logika;

namespace Turanszki_Kristof_Beadando.Palyaelemek.Powerupok
{
    /// <summary>
    /// Puska típusú powerup
    /// </summary>
    internal class Puska : Powerup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Puska" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public Puska(Point hely)
        {
            Hely = hely;
        }

        /// <summary>
        /// Ez a metódus felelős a külső eléréshez, és a kép nevét itt adjuk át.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return GetBrush("Puska.png");
            }
        }

        /// <summary>
        /// Meghívja a PalyaModelnek a betaraz metódusát.
        /// </summary>
        /// <param name="vM">VM átadása</param>
        public override void Aktival(PalyaModel vM)
        {
            vM.Betaraz();
        }
    }
}
