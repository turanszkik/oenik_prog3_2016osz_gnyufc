﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Turanszki_Kristof_Beadando.Interface;
using Turanszki_Kristof_Beadando.Logika;

namespace Turanszki_Kristof_Beadando.Palyaelemek.Powerupok
{
    /// <summary>
    /// Absztrakt osztály a poweruppoknak, melyfől a későbbi objektumokat származtatjuk.
    /// </summary>
    internal abstract class Powerup : IUtkozhet
    {
        /// <summary>
        /// Powerup szélessége
        /// </summary>
        private const int SZELESSEG = Fal.SZELESSEG;

        /// <summary>
        /// Powerup magassága
        /// </summary>
        private const int MAGASSAG = Fal.MAGASSAG;

        /// <summary>
        /// Ütközés szempontjából használt
        /// </summary>
        private Geometry geometry;

        /// <summary>
        /// A powerup helye
        /// </summary>
        private Point hely;

        /// <summary>
        /// Esik-e a powerup?
        /// true = esik
        /// false = nem esik
        /// </summary>
        private bool esik = false;

        /// <summary>
        /// Getter Setter a Hely értékhez.
        /// </summary>
        public Point Hely
        {
            get
            {
                return hely;
            }

            set
            {
                hely = value;
            }
        }

        /// <summary>
        /// Getter a szélesség értékhez
        /// </summary>
        public int Szelesseg
        {
            get
            {
                return SZELESSEG;
            }
        }

        /// <summary>
        /// Getter a magasság értékhez
        /// </summary>
        public int Magassag
        {
            get
            {
                return MAGASSAG;
            }
        }

        /// <summary>
        /// Getter Setter az esik értékhez.
        /// </summary>
        public bool Esik
        {
            get
            {
                return esik;
            }

            set
            {
                esik = value;
            }
        }

        /// <summary>
        /// Itt adja át a kép nevét.
        /// </summary>
        public abstract Brush Brush { get; }

        /// <summary>
        /// Visszaadja minden fel elem geometry-jét
        /// </summary>
        public Geometry Geometry
        {
            get
            {
                geometry = new RectangleGeometry(new Rect(Hely.X, Hely.Y, SZELESSEG, MAGASSAG));
                return geometry;
            }
        }

        /// <summary>
        /// Az elemek megjelenítéséért felelős metódus
        /// </summary>
        /// <param name="nev">A magjelenítendő kép neve</param>
        /// <returns>A brush-t adja vissza</returns>
        public Brush GetBrush(string nev)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(nev, UriKind.Relative)));
            ib.TileMode = TileMode.Tile; 
            ib.Viewport = new Rect(Hely.X, Hely.Y, SZELESSEG, MAGASSAG);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }

        /// <summary>
        /// Ha eltalálja valami, akkor "elkezd" esni, azaz az esik értéke true-ra változik.
        /// </summary>
        public void Eltalal()
        {
            this.Esik = true;
        }

        /// <summary>
        /// Az aktival metódus hajtja végre a powerup felszedésével kapcsolatos módosításokat.
        /// </summary>
        /// <param name="vM">Értékül kapja a PalyaModel osztályt, melyben a módosításokat hajtja végre</param>
        public abstract void Aktival(PalyaModel vM);

        /// <summary>
        /// Ez a metódus módosítja a powerup helyét, ha az esik.
        /// </summary>
        public void Mozog()
        {
            if (Esik == true)
            {
                hely.Y += 3;
            }
        }
    }
}
