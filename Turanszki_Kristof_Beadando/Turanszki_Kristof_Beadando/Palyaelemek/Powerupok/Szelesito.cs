﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Turanszki_Kristof_Beadando.Logika;

namespace Turanszki_Kristof_Beadando.Palyaelemek.Powerupok
{
    /// <summary>
    /// Szélesítő típusú powerup
    /// </summary>
    internal class Szelesito : Powerup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Szelesito" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public Szelesito(Point hely)
        {
            this.Hely = hely;
        }

        /// <summary>
        /// Ez a metódus felelős a külső eléréshez, és a kép nevét itt adjuk át.
        /// </summary>
        public override Brush Brush
        {
            get
            {
                return GetBrush("Szelesito.png");
            }
        }

        /// <summary>
        /// Megnöveli az ütő méretét, 30-al és a bal oldalt elhelyezkedő töltények helyéhez is hozzáad 30-at (az x koordinátához)
        /// </summary>
        /// <param name="vM">VM átadása</param>
        public override void Aktival(PalyaModel vM)
        {
            vM.Uto.Szelesseg += 30;
            for (int i = 1; i < vM.Toltenyek.Count; i++)
            {
                vM.Toltenyek[i].ModisitX(30);
                i++;
            }
        }
    }
}
