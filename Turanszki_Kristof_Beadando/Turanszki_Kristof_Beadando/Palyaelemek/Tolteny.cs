﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Turanszki_Kristof_Beadando.Interface;

namespace Turanszki_Kristof_Beadando.Palyaelemek
{
    /// <summary>
    /// Töltények adatait tároló osztály
    /// </summary>
    internal class Tolteny : IUtkozhet
    {
        /// <summary>
        /// Szélességet tárolja
        /// </summary>
        public const int SZELESSEG = 10;

        /// <summary>
        /// Magasságot tárolja
        /// </summary>
        private const int MAGASSAG = Uto.MAGASSAG;

        /// <summary>
        /// Ütközés szempontjából használt
        /// </summary>
        private Geometry geometry;

        /// <summary>
        /// Helyet tárolja
        /// </summary>
        private Point hely;

        /// <summary>
        /// Mozgást tárolja
        /// True = mozog
        /// False = nem mozog
        /// </summary>
        private bool mozog = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tolteny" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="hely">Kezdeti hely megadása</param>
        public Tolteny(Point hely)
        {
            Hely = hely;
        }

        /// <summary>
        /// Getter Setter a hely lekérdezésére, beállítására
        /// </summary>
        public Point Hely
        {
            get
            {
                return hely;
            }

            set
            {
                hely = value;
            }
        }

        /// <summary>
        /// Getter a magasság lekérdezésére
        /// </summary>
        public int Magassag
        {
            get
            {
                return MAGASSAG;
            }
        }

        /// <summary>
        /// Getter a szélesség lekérdezésére
        /// </summary>
        public int Szelesseg
        {
            get
            {
                return SZELESSEG;
            }
        }

        /// <summary>
        /// Getter Setter a mozgás lekérdezésére, beállítására
        /// </summary>
        public bool Mozog
        {
            get
            {
                return mozog;
            }

            set
            {
                mozog = value;
            }
        }

        /// <summary>
        /// Az elemek megjelenítéséért felelős
        /// </summary>
        public Brush Brush
        {
            get
            {
                ImageBrush ib = new ImageBrush(new BitmapImage(new Uri("Tolteny.png", UriKind.Relative)));
                ib.TileMode = TileMode.Tile;
                ib.Viewport = new Rect(Hely.X, Hely.Y, Szelesseg, MAGASSAG);
                ib.ViewportUnits = BrushMappingMode.Absolute;
                return ib;
            }
        }

        /// <summary>
        /// Visszaadja minden fel elem geometry-jét
        /// </summary>
        public Geometry Geometry
        {
            get
            {
                geometry = new RectangleGeometry(new Rect(Hely.X, Hely.Y, SZELESSEG, MAGASSAG));
                return geometry;
            }
        }

        /// <summary>
        /// A mozgást megvalósító metódus
        /// </summary>
        public void Mozgas()
        {
            if (Mozog == true)
            {
                hely.Y -= 5;
            }
        }

        /// <summary>
        /// A hely X koordinátáját módosító metódus
        /// </summary>
        /// <param name="x">Ennyivel módosítja az értéket</param>
        public void ModisitX(int x)
        {
            hely.X += x;
        }

        /// <summary>
        /// Az ütővel való mozgáshoz szükséges metódus
        /// </summary>
        /// <param name="diff">Ennyivel módosítja az értéket</param>
        public void Helymodosit(double diff)
        {
            hely.X -= diff;
        }
    }
}
