﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Turanszki_Kristof_Beadando.Palyaelemek
{
    /// <summary>
    /// A pálya háttérképét tárolja, és annak adatait
    /// </summary>
    internal class Hatterkep
    {
        /// <summary>
        /// A háttérkép szélessége.
        /// </summary>
        public const int SZELESSEG = 1400;

        /// <summary>
        /// A háttérkép magassága.
        /// </summary>
        public const int MAGASSAG = 760;

        /// <summary>
        /// Ez alapján fogja betölteni a megfelelő file-t.
        /// </summary>
        private string nev;

        /// <summary>
        /// Initializes a new instance of the <see cref="Hatterkep" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="name">Kezdeti név megadása</param>
        public Hatterkep(string name)
        {
            nev = name;
        }

        /// <summary>
        /// Az elemek megjelenítéséért felelős metódus
        /// </summary>
        public Brush Brush
        {
            get
            {
                ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(nev, UriKind.Relative)));
                ib.TileMode = TileMode.Tile;
                ib.Viewport = new Rect(0, 0, SZELESSEG, MAGASSAG);
                ib.ViewportUnits = BrushMappingMode.Absolute;
                return ib;
            }
        }
    }
}
