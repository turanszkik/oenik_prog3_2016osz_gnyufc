﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Turanszki_Kristof_Beadando.Interface;

namespace Turanszki_Kristof_Beadando.Palyaelemek
{
    /// <summary>
    /// A pajzs adatait eltárolni hivatot osztály
    /// </summary>
    internal class Pajzs : IUtkozhet
    {
        /// <summary>
        /// Szélességet tárolja
        /// </summary>
        private const int SZELESSEG = Hatterkep.SZELESSEG;

        /// <summary>
        /// Magasságot tárolja
        /// </summary>
        private const int MAGASSAG = 10;

        /// <summary>
        /// Helyet tárolja
        /// </summary>
        private Point hely;

        /// <summary>
        /// Ütközés szempontjából használt
        /// </summary>
        private Geometry geometry;

        /// <summary>
        /// Initializes a new instance of the <see cref="Pajzs" /> class
        /// Konstruktor, alapvető értékek beállítására szolgál.
        /// </summary>
        public Pajzs()
        {
            Hely = new Point(0, Hatterkep.MAGASSAG - 40);
        }

        /// <summary>
        /// Getter a szélesség lekérdezésére
        /// </summary>
        public int Szelesseg
        {
            get
            {
                return SZELESSEG;
            }
        }

        /// <summary>
        /// getter a magasság lekérdezésére
        /// </summary>
        public int Magassag
        {
            get
            {
                return MAGASSAG;
            }
        }

        /// <summary>
        /// Getter Setter a hely lekérdezésére és megadására.
        /// </summary>
        public Point Hely
        {
            get
            {
                return hely;
            }

            set
            {
                hely = value;
            }
        }

        /// <summary>
        /// Az elemek megjelenítéséért felelős
        /// </summary>
        public Brush Brush
        {
            get
            {
                ImageBrush ib = new ImageBrush(new BitmapImage(new Uri("Pajzs.png", UriKind.Relative)));
                ib.TileMode = TileMode.Tile;
                ib.Viewport = new Rect(Hely.X, Hely.Y, Szelesseg, MAGASSAG);
                ib.ViewportUnits = BrushMappingMode.Absolute;
                return ib;
            }
        }

        /// <summary>
        /// Visszaadja minden fel elem geometry-jét
        /// </summary>
        public Geometry Geometry
        {
            get
            {
                if (geometry == null)
                {
                    geometry = new RectangleGeometry(new Rect(Hely.X, Hely.Y, SZELESSEG, MAGASSAG));
                }

                return geometry;
            }
        }
    }
}
