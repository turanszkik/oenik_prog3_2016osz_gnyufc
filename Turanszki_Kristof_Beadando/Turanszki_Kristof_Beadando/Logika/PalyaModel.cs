﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Turanszki_Kristof_Beadando.Interface;
using Turanszki_Kristof_Beadando.Palyaelemek;
using Turanszki_Kristof_Beadando.Palyaelemek.Powerupok;

namespace Turanszki_Kristof_Beadando.Logika
{
    /// <summary>
    /// Ez az osztály felelős minden kirajzolandó, és nem kirajzolandó elem nyilvántartására, egyes műveletek végrehajtására.
    /// </summary>
    internal class PalyaModel : Bindable
    {
        /// <summary>
        /// Ez a lista tartalmazza az összes fal elemet, melyet a játékban szét kell törni.
        /// </summary>
        private List<Fal> falak = new List<Fal>();

        /// <summary>
        /// Ez a lista tartalmazza az összes powerupot, melyet az adott pályán megszerezhetünk.
        /// </summary>
        private List<Powerup> powerupok = new List<Powerup>();

        /// <summary>
        /// Ez a lista tartalmazza a töltényeket a Puska powerup felvétele esetén.
        /// </summary>
        private List<Tolteny> toltenyek = new List<Tolteny>();

        /// <summary>
        /// Ez a lista tartalmazza a labdáinkat.
        /// </summary>
        private List<Labda> labdak = new List<Labda>();

        /// <summary>
        /// Ez tartalmazza az adott pályához tatozó háttérképet, és megjelenítéséhez használt metódusokat.
        /// </summary>
        private Hatterkep hatter;

        /// <summary>
        /// Ez tartalmazza az adott pályához tatozó ütőt, és megjelenítéséhez használt metódusokat.
        /// </summary>
        private Uto uto;

        /// <summary>
        /// Ha esetleg felvettünk egy pajzsot, ez az objektum tárolja el az adatait.
        /// </summary>
        private Pajzs pajzs;

        /// <summary>
        /// Az életek számát tároljuk.
        /// </summary>
        private int eletek;

        /// <summary>
        /// A megszerzett pontok számát tároljuk.
        /// </summary>
        private int pontok;

        /// <summary>
        /// A széttörhető elemek darabszámát tároljuk, erre azért van szükség, hogy ha a játékos az összeset széttörte, akkor a pályának vége.
        /// </summary>
        private int szettorhetoElemek;

        /// <summary>
        /// Pálya sorszámának tárolása
        /// </summary>
        private int szam;

        /// <summary>
        /// Initializes a new instance of the <see cref="PalyaModel" /> class
        /// Konstruktok, alapvető értékek beállítására szolgál.
        /// </summary>
        /// <param name="pontok">Eddigi pontszám átadása</param>
        /// <param name="eletek">Eddigi eletek számainak átadása</param>
        /// <param name="szam">Pálya számának átadása</param>
        public PalyaModel(int pontok, int eletek, int szam)
        {
            this.eletek = eletek;
            this.pontok = pontok;
            Szam = szam;
            Uto = new Uto(new Point(600, 700));
            this.Labdak.Add(new Labda(new Point(700, 700 - Labda.MERET)));
            this.Hatter = new Hatterkep("bg" + Szam + ".jpg");
            this.Betolt(Szam + ".lvl");
        }

        /// <summary>
        /// Szükséges az adatkötéshez, mert a XAML-ben erre hivatkozunk.
        /// </summary>
        public PalyaModel Self
        {
            get { return this; }
        }

        /// <summary>
        /// A falak lista módisítására, lekérdezésére szolgál (az osztályon kívülről)
        /// </summary>
        public List<Fal> Falak
        {
            get
            {
                return this.falak;
            }

            set
            {
                this.falak = value;
            }
        }

        /// <summary>
        /// Az ütő módisítására, lekérdezésére szolgál.
        /// </summary>
        public Uto Uto
        {
            get
            {
                return this.uto;
            }

            set
            {
                this.uto = value;
            }
        }

        /// <summary>
        /// A háttérkép módisítására, lekérdezésére szolgál
        /// </summary>
        public Hatterkep Hatter
        {
            get
            {
                return this.hatter;
            }

            set
            {
                this.hatter = value;
            }
        }

        /// <summary>
        /// A poweruppok lista módosítására, lekérdezésére szolgál
        /// </summary>
        public List<Powerup> Powerupok
        {
            get
            {
                return this.powerupok;
            }

            set
            {
                this.powerupok = value;
            }
        }

        /// <summary>
        /// A pajzs módosítására, lekérdezésére szolgál
        /// </summary>
        public Pajzs Pajzs
        {
            get
            {
                return this.pajzs;
            }

            set
            {
                this.pajzs = value;
            }
        }

        /// <summary>
        /// A pontok módosítására, lekérdezésére szolgál.
        /// </summary>
        public int Pontok
        {
            get
            {
                return this.pontok;
            }

            set
            {
                SetProperty(ref pontok, value);
            }
        }

        /// <summary>
        /// Az életek módosítására, lekérdezésére szolgál
        /// </summary>
        public int Eletek
        {
            get
            {
                return this.eletek;
            }

            set
            {
                SetProperty(ref eletek, value);
            }
        }

        /// <summary>
        /// A széttörhető elemek számolására való mező.
        /// Mivel a fal elemek között fém fal is van, amit nem lehet széttörni, ezért mikor az összes törhető elem eltűnik, akkor van vége a pályának.
        /// </summary>
        public int SzettorhetoElemek
        {
            get
            {
                return szettorhetoElemek;
            }

            set
            {
                szettorhetoElemek = value;
            }
        }

        /// <summary>
        /// A pálya számának beállítása, lekérdezése
        /// </summary>
        public int Szam
        {
            get
            {
                return szam;
            }

            set
            {
                szam = value;
            }
        }

        /// <summary>
        /// A töltények lista módisítására lekérdezésére szolgál.
        /// </summary>
        internal List<Tolteny> Toltenyek
        {
            get
            {
                return this.toltenyek;
            }

            set
            {
                this.toltenyek = value;
            }
        }

        /// <summary>
        /// A labdál kista Módosítására, lekérdezésére szolgál.
        /// </summary>
        internal List<Labda> Labdak
        {
            get
            {
                return this.labdak;
            }

            set
            {
                this.labdak = value;
            }
        }

        /// <summary>
        /// A Puska powerup felvételével hozzáad összesen 10 töltényt a toltenyek listánkhoz.
        /// </summary>
        public void Betaraz()
        {
            Point seged;
            for (int i = 0; i < 5; i++)
            {
                seged = new Point(this.Uto.Hely.X - Tolteny.SZELESSEG, this.Uto.Hely.Y);
                this.Toltenyek.Add(new Tolteny(seged));
                seged = new Point(this.Uto.Hely.X + Uto.Szelesseg, this.Uto.Hely.Y);
                this.Toltenyek.Add(new Tolteny(seged));
            }
        }

        /// <summary>
        /// 2 töltényt "kilő", ha van töltényünk.
        /// </summary>
        public void ToltenyIndit()
        {
            int i = 0;
            while (i < this.Toltenyek.Count && this.Toltenyek[i].Mozog == true)
            {
                i++;
            }

            if (i < this.Toltenyek.Count)
            {
                this.Toltenyek[i].Mozog = true;
                this.Toltenyek[i + 1].Mozog = true;
            }
        }

        /// <summary>
        /// A labda mozgásáért felelős.
        /// </summary>
        public void Labdamozog()
        {
            for (int i = 0; i < this.Labdak.Count; i++)
            {
                this.Labdak[i].Mozog();
            }

            this.OnPropertyChanged("Self");
        }

        /// <summary>
        /// Betölti az adott sorszámú pályát file-ból
        /// </summary>
        /// <param name="sorszam">A pálya sorszáma</param>
        public void Betolt(string sorszam)
        {
            string[] palya = File.ReadAllLines(sorszam);
            for (int x = 0; x < 6; x++)
            {
                for (int y = 0; y < palya[x].Length; y++)
                {
                    switch (palya[x][y])
                    {
                        case 'X':
                            this.Falak.Add(new TeglaFal(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x + 1) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'K':
                            this.Falak.Add(new KoFal(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x + 1) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'B':
                            this.Falak.Add(new Bomba(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x + 1) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'F':
                            this.Falak.Add(new FemFal(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x + 1) * (Fal.MAGASSAG + 10))));
                            this.SzettorhetoElemek--;
                            break;
                    }
                }
            }

            this.SzettorhetoElemek += this.Falak.Count();
            for (int x = 6; x < palya.Length; x++)
            {
                for (int y = 0; y < palya[x].Length; y++)
                {
                    switch (palya[x][y])
                    {
                        case 'D':
                            this.Powerupok.Add(new Duplazo(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x - 6) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'E':
                            this.Powerupok.Add(new Erme(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x - 6) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'L':
                            this.Powerupok.Add(new ExtraElet(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x - 6) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'G':
                            this.Powerupok.Add(new Gyorsito(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x - 6) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'K':
                            this.Powerupok.Add(new Keskenyito(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x - 6) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'P':
                            this.Powerupok.Add(new PajzsPup(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x - 6) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'S':
                            this.Powerupok.Add(new Szelesito(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x - 6) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'M':
                            this.Powerupok.Add(new Magnes(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x - 6) * (Fal.MAGASSAG + 10))));
                            break;
                        case 'R':
                            this.Powerupok.Add(new Puska(new Point((y * (Fal.SZELESSEG + 10)) + 10, (x - 6) * (Fal.MAGASSAG + 10))));
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// A labdának az iránymódisítását hivatott elvégezni.
        /// </summary>
        /// <param name="i">A labda lista éppen ellenőrzött elemének sorszáma</param>
        public void LabdaIranyModositas(int i)
        {
            int leptek = uto.Szelesseg / 5;
            double utoBalX = uto.Hely.X;
            Vector v = new Vector();
            if (Labdak[i].Hely.X < utoBalX + leptek)
            {
                v.X = -1;
            }
            else if (Labdak[i].Hely.X < utoBalX + (leptek * 2))
            {
                v.X = -0.5;
            }
            else if (Labdak[i].Hely.X < utoBalX + (leptek * 3))
            {
            }
            else if (Labdak[i].Hely.X < utoBalX + (leptek * 4))
            {
                v.X = +0.5;
            }
            else
            {
                v.X = +1;
            }

            Labdak[i].Irany += v;
        }
    }
}