﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Turanszki_Kristof_Beadando.Logika
{
    /// <summary>
    /// Azt teszi lehetővé, gogy az adatközéssel létrehozott grafikánk frissüljön.
    /// </summary>
    internal class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// Erre az eventre iratkoznak fel az egyes objektumok az XML-ben
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// A metódus meghívásával hívjuk megaz eventünket, a megfelelő paraméterekkel, 
        /// </summary>
        /// <param name="name">A frissítendő objektum neve</param>
        protected void OnPropertyChanged(string name = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Paraméteradásnál fontos, hog ha ezzel adjuk meg, az adatkötés által frissül.
        /// </summary>
        /// <typeparam name="T">Típus megadás</typeparam>
        /// <param name="field">Minek az értékét változtatjuk</param>
        /// <param name="newvalue">Mivé változtatjuk</param>
        /// <param name="name">A frissítendő objektum neve</param>
        protected void SetProperty<T>(ref T field, T newvalue, [CallerMemberName] string name = null)
        {
            field = newvalue;
            OnPropertyChanged(name);
        }
    }
}