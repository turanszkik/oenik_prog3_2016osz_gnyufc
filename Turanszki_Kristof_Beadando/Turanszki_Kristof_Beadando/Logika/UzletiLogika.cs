﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Turanszki_Kristof_Beadando.Interface;
using Turanszki_Kristof_Beadando.Palyaelemek;

namespace Turanszki_Kristof_Beadando.Logika
{
    /// <summary>
    /// A PalyaModel-től kicsit távolabbi logikákat vizsgáljuk, ugyanakkor a frissítések ütemezését is itt végezzük.
    /// </summary>
    internal class UzletiLogika : Bindable
    {
        /// <summary>
        /// A PalyaModel inicializálása
        /// </summary>
        private PalyaModel vM;

        /// <summary>
        /// A vonzás ütemezőnek az inicializálása.
        /// </summary>
        private DispatcherTimer vonzas;

        /// <summary>
        /// A többi ütemező nicializálása.
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="UzletiLogika" /> class.
        /// Konstruktor
        /// </summary>
        public UzletiLogika()
        {
            VM = new PalyaModel(0, 0, 1);

            vonzas = new DispatcherTimer();
            vonzas.Interval = TimeSpan.FromMilliseconds(10);
            vonzas.Tick += LabdaVonzas;

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(15);
            timer.Tick += Tick;
            timer.Start();
        }

        /// <summary>
        /// Játék állását magának a MainWindownak küdő eventhandler.
        /// </summary>
        /// <param name="kilep">Kilépjen-e a program abagy ne (true/false)</param>
        public delegate void PalyaBetoltEventHandler(bool kilep);

        /// <summary>
        /// Erre az eseményre fog feliratkozni a MainWindow.
        /// </summary>
        public event PalyaBetoltEventHandler PalyaBetolt;

        /// <summary>
        /// A PalyaModel-t adja át, és állítja be.
        /// </summary>
        public PalyaModel VM
        {
            get
            {
                return vM;
            }

            set
            {
                vM = value;
            }
        }

        /// <summary>
        /// Az ütő mozgatásáért felelős metódus, mely az egérkurzor pozícióját veszi alapul
        /// </summary>
        /// <param name="hely">Az egérkurzor pozíciója</param>
        public void UtoMozgas(Point hely)
        {
            Uto uto = VM.Uto;
            double diff = uto.Hely.X;
            uto.Hely = new Point(hely.X, uto.Hely.Y);
            diff -= uto.Hely.X;
            VM.Uto = uto;
            for (int i = 0; i < VM.Labdak.Count; i++)
            {
                if (!VM.Labdak[i].Halad)
                {
                    Point pont = VM.Labdak[i].Hely;
                    pont.X -= diff;
                    VM.Labdak[i].Hely = pont;
                }
            }

            if (VM.Toltenyek != null)
            {
                foreach (Tolteny tolteny in VM.Toltenyek)
                {
                    if (!tolteny.Mozog)
                    {
                        tolteny.Helymodosit(diff);
                    }
                }
            }
        }

        /// <summary>
        /// A poweruppok mozgatására szolgáláó metódus.
        /// </summary>
        public void PowerupMozog()
        {
            for (int i = 0; i < this.vM.Powerupok.Count; i++)
            {
                if (this.vM.Powerupok[i].Hely.Y + this.vM.Powerupok[i].Magassag < Hatterkep.MAGASSAG)
                {
                    this.vM.Powerupok[i].Mozog();
                }
                else
                {
                    this.vM.Powerupok.Remove(this.vM.Powerupok[i]);
                    i--;
                }
            }
        }

        /// <summary>
        /// A töltények mozgatására szolgál, amennyibel van töltény.
        /// </summary>
        public void ToltenyMozog()
        {
            if (this.vM.Toltenyek != null)
            {
                for (int i = 0; i < this.vM.Toltenyek.Count; i++)
                {
                    this.vM.Toltenyek[i].Mozgas();
                    if (this.vM.Toltenyek[i].Hely.Y <= 0)
                    {
                        this.vM.Toltenyek.Remove(this.vM.Toltenyek[i]);
                    }
                }
            }
        }

        /// <summary>
        /// A labda mozgásának indításáért felelős metódus.
        /// </summary>
        public void LabdaIndit()
        {
            for (int i = 0; i < VM.Labdak.Count; i++)
            {
                VM.Labdak[i].Halad = true;
            }
        }

        /// <summary>
        /// Elindítja a labda vonzását.
        /// </summary>
        public void LabdavonzasIndit()
        {
            vonzas.Start();
        }

        /// <summary>
        /// Leállítja a labda vonzását.
        /// </summary>
        public void LabdavonzasMegallit()
        {
            vonzas.Stop();
        }

        /// <summary>
        /// Azt vizsgálja, hogy a két elem egy vonalban vannak-e vertikálisan
        /// </summary>
        /// <param name="elem">Egyik elem, amit vizsgálunk (ha bombát vizsgálunk, ennek kell lennie a bombának)</param>
        /// <param name="powerup">Másik elem amit vizsgálunk</param>
        /// <param name="bomba">Ha bomba robbanást vizsgálunk, ez az érék true, egyébként false marad</param>
        /// <returns>True = ha egy vonalban vannak, False = ha nincsenek egy vonalban.</returns>
        private bool Utkozik(IUtkozhet elem, IUtkozhet powerup, bool bomba = false)
        {
            if (bomba == false)
            {
                if (Geometry.Combine(elem.Geometry, powerup.Geometry, GeometryCombineMode.Intersect, null).GetArea() > 0)
                {
                    return true;
                }
            }
            else
            {
                Geometry g = new RectangleGeometry(new Rect(elem.Hely.X - 15, elem.Hely.Y - 15, elem.Szelesseg + 30, elem.Magassag + 30));
                if (Geometry.Combine(g, powerup.Geometry, GeometryCombineMode.Intersect, null).GetArea() > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Ez a metódus azt ellenőrzi, hogy az egyes elemek ütköznek-e egymással.
        /// Ha a golyó ütközik valamivel, akkor az irányát módosítjuk.
        ///     Ha fallal ütközik, a fal eltűnik vagy nem, attól függően, hogy milyen típusú.
        /// Ha Powerup ütközik az ütővel, akkor azt eltüntetjük (töröljük a listából).
        /// </summary>
        private void Talalat()
        {
            for (int i = 0; i < vM.Labdak.Count; i++)
            {
                if (vM.Pajzs != null && Utkozik(vM.Pajzs, vM.Labdak[i]))
                {
                    Vector v = vM.Labdak[i].Irany;
                    v.Y = -vM.Labdak[i].Irany.Y;
                    vM.Labdak[i].Irany = v;
                    vM.Pajzs = null;
                }
            }

            for (int i = 0; i < vM.Powerupok.Count; i++)
            {
                if (Utkozik(vM.Uto, vM.Powerupok[i]))
                {
                    vM.Powerupok[i].Aktival(vM);
                    vM.Powerupok.Remove(vM.Powerupok[i]);
                    i--;
                }
            }

            for (int i = 0; i < vM.Toltenyek.Count; i++)
            {
                if (vM.Toltenyek[i].Mozog)
                {
                    for (int j = 0; j < vM.Powerupok.Count; j++)
                    {
                        if (Utkozik(vM.Powerupok[j], vM.Toltenyek[i]))
                        {
                            vM.Powerupok[j].Esik = true;
                            break;
                        }
                    }

                    for (int j = 0; j < vM.Falak.Count; j++)
                    {
                        if (Utkozik(vM.Falak[j], vM.Toltenyek[i]))
                        {
                            if (vM.Falak[j].Torik())
                            {
                                vM.Falak.Remove(vM.Falak[j]);
                                vM.SzettorhetoElemek--;
                                vM.Pontok++;
                            }

                            j = vM.Falak.Count;
                            vM.Toltenyek.Remove(vM.Toltenyek[i]);
                            i--;
                        }
                    }
                }
            }

            for (int j = 0; j < vM.Labdak.Count; j++)
            {
                for (int i = 0; i < vM.Powerupok.Count; i++)
                {
                    if (Utkozik(vM.Powerupok[i], vM.Labdak[j]))
                    {
                        vM.Powerupok[i].Eltalal();
                    }
                }
            }

            for (int i = 0; i < vM.Falak.Count; i++)
            {
                for (int j = 0; j < vM.Labdak.Count; j++)
                {
                    try
                    {
                        if (Utkozik(vM.Falak[i], vM.Labdak[j]))
                        {
                            Point regiHely = vM.Labdak[j].Hely;
                            Vector regiIrany = vM.Labdak[j].Irany;
                            Vector v = vM.Labdak[j].Irany;
                            v.X = -vM.Labdak[j].Irany.X;
                            vM.Labdak[j].Irany = v;
                            vM.Labdak[j].Mozog();
                            if (Utkozik(vM.Falak[i], vM.Labdak[j]))
                            {
                                vM.Labdak[j].Irany = regiIrany;
                                v = vM.Labdak[j].Irany;
                                v.Y = -vM.Labdak[j].Irany.Y;
                                vM.Labdak[j].Irany = v;
                                vM.Labdak[j].Mozog();
                            }

                            if (vM.Falak[i] is Bomba)
                            {
                                for (int x = 0; x < vM.Falak.Count; x++)
                                {
                                    if (Utkozik(vM.Falak[i], vM.Falak[x], true) && x != i)
                                    {
                                        vM.Falak.Remove(vM.Falak[x]);
                                        vM.SzettorhetoElemek--;
                                        vM.Pontok++;
                                        x--;
                                        if (x < i)
                                        {
                                            i--;
                                        }
                                    }
                                }

                                for (int x = 0; x < vM.Powerupok.Count; x++)
                                {
                                    if (Utkozik(vM.Falak[i], vM.Powerupok[x], true))
                                    {
                                        vM.Powerupok[x].Esik = true;
                                    }
                                }
                            }

                            if (vM.Falak[i].Torik())
                            {
                                vM.Falak.Remove(vM.Falak[i]);
                                vM.SzettorhetoElemek--;
                                vM.Pontok++;
                                i--;
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            for (int i = 0; i < vM.Labdak.Count; i++)
            {
                if (Utkozik(vM.Uto, vM.Labdak[i]))
                {
                    Point regiHely = vM.Labdak[i].Hely;
                    Vector regiIrany = vM.Labdak[i].Irany;
                    Vector v = vM.Labdak[i].Irany;
                    v.X = -vM.Labdak[i].Irany.X;
                    vM.Labdak[i].Irany = v;
                    vM.Labdak[i].Mozog();
                    if (Utkozik(vM.Uto, vM.Labdak[i]))
                    {
                        regiHely.Y = vM.Uto.Hely.Y - Labda.MERET;
                        vM.Labdak[i].Hely = regiHely;
                        vM.Labdak[i].Irany = regiIrany;
                        v = vM.Labdak[i].Irany;
                        v.Y = -vM.Labdak[i].Irany.Y;
                        vM.Labdak[i].Irany = v;
                        vM.Labdak[i].Mozog();
                        vM.LabdaIranyModositas(i);
                    }

                    if (vM.Uto.Magnes)
                    {
                        vM.Labdak[i].Halad = false;
                        vM.Uto.Magnes = false;
                    }
                }
            }
        }

        /// <summary>
        /// A frissítések összefoglalása egy metódusba
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Tick(object sender, EventArgs e)
        {
            VM.Labdamozog();
            Talalat();
            PowerupMozog();
            LabdaLeesett();
            KovetkezoPalya();
            ToltenyMozog();
        }

        /// <summary>
        /// Ha a pályán a széttörhető elemek száma 0, akkor a játékos a következő pályaára léphet
        /// </summary>
        private void KovetkezoPalya()
        {
            if (vM.SzettorhetoElemek < 1)
            {
                if (vM.Szam < 6)
                {
                    int pont = VM.Pontok;
                    int elet = VM.Eletek;
                    int palya = VM.Szam;
                    VM = new PalyaModel(pont, elet, palya + 1);
                    PalyaBetolt(false);
                }
                else
                {
                    if (MessageBox.Show("Megnyerted a játékot! :)\nElért pontszám: " + vM.Pontok + "\nÚjra akarod kezdeni?", "Congrats", MessageBoxButton.YesNo) == MessageBoxResult.Yes) 
                    {
                        vM = new PalyaModel(0, 0, 1);
                        PalyaBetolt(false);
                    }
                    else
                    {
                        PalyaBetolt(true);
                    }
                }
            }
        }

        /// <summary>
        /// Azt vizsgálja, hogy a labda földet ért-e. Ha ige, további vizsgálatokat végez:
        ///     Van-e még labda
        ///     Van-e még élet
        /// </summary>
        private void LabdaLeesett()
        {
            for (int i = 0; i < VM.Labdak.Count; i++)
            {
                if (VM.Labdak.Count > 1 && VM.Labdak[i].Hely.Y + Labda.MERET >= Hatterkep.MAGASSAG)
                {
                    VM.Labdak.Remove(VM.Labdak[i]);
                }
            }

            if (VM.Labdak.Count == 1 && VM.Labdak[0].Hely.Y + Labda.MERET >= Hatterkep.MAGASSAG)
            {
                if (VM.Eletek == 0)
                {
                    if (MessageBox.Show("Elvesztetted a játékot :(\nÚjra akarod kezdeni?", "Game Over", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        PalyaBetolt(false);
                    }
                    else
                    {
                        PalyaBetolt(true);
                    }

                    timer.Stop();
                }
                else
                {
                    VM.Eletek--;
                    Point pont = VM.Uto.Kozep;
                    pont.Y = 700 - Labda.MERET;
                    VM.Labdak.Add(new Labda(pont));
                }
            }
        }

        /// <summary>
        /// Az ütő felé vonzza a labdákat
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void LabdaVonzas(object sender, EventArgs e)
        {
            for (int i = 0; i < VM.Labdak.Count; i++)
            {
                Vector vektor = new Vector();
                vektor.X = VM.Uto.Kozep.X - VM.Labdak[i].Hely.X;
                vektor.Y = VM.Uto.Kozep.Y - VM.Labdak[i].Hely.Y;
                double hossz = vektor.Length * 20;
                vektor.X = vektor.X / hossz;
                vektor.Y = vektor.Y / hossz;
                VM.Labdak[i].Irany += vektor;
            }
        }
    }
}
