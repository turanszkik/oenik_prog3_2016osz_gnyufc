﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Turanszki_Kristof_Beadando.Interface
{
    /// <summary>
    /// Ez az interface teszi lehetővé, hogy két objektum közötti érintkezést tudjuk vizsgálni.
    /// </summary>
    internal interface IUtkozhet
    {
        /// <summary>
        /// Gets: Visszatérési értéke az objektum szélessége lesz.
        /// </summary>
        int Szelesseg { get; }

        /// <summary>
        /// Gets: Visszatérési értéke az objektum magassága lesz.
        /// </summary>
        int Magassag { get; }

        /// <summary>
        /// Gets: Az objektum helyét határozza meg.
        /// </summary>
        Point Hely { get; }

        /// <summary>
        /// Visszaadja a geometry-jét az elemnek.
        /// </summary>
        /// <returns></returns>
        Geometry Geometry { get; }
    }
}
