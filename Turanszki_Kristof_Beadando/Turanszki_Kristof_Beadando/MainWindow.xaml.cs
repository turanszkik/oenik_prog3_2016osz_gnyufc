﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Turanszki_Kristof_Beadando.Logika;
using Turanszki_Kristof_Beadando.Palyaelemek;

namespace Turanszki_Kristof_Beadando
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Üzeti logika
        /// </summary>
        private UzletiLogika uL;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow" /> class
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Azok a parancsok, melyeket a program végrehajt betöltéskor
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            uL = new UzletiLogika();
            Betolt(false);
        }

        /// <summary>
        /// A betölt metódis, ami feliratkozik az ÜzletiLogika eseményére
        /// </summary>
        /// <param name="kilep">The parameter is not used.</param>
        private void Betolt(bool kilep)
        {
            if (kilep)
            {
                this.Close();
            }
            else
            {
                if (uL != null)
                {
                    uL.PalyaBetolt -= Betolt;
                }

                DataContext = uL.VM;
                uL.PalyaBetolt += Betolt;
            }
        }

        /// <summary>
        /// Egérmozgásnál ez az esemény fut le
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            Point s = e.GetPosition(this);
            uL.UtoMozgas(s);
        }

        /// <summary>
        /// Az egér bal gombjának lenyomásakor ez az utasítás hajtódik végre
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            uL.LabdavonzasIndit();
            uL.LabdaIndit();
            uL.VM.ToltenyIndit();
        }

        /// <summary>
        /// Az egér bal gombjának felemelkedésekor ez az utasítás hajtódik végre
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            uL.LabdavonzasMegallit();
        }
    }
}
